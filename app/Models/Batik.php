<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Batik extends Model
{
    protected $fillable = [
        'nama_batik', 'motif_batik', 'deskripsi_batik', 'asal_batik', 'gambar_batik'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($batik) {
            $batik->kode_batik = 'BTK' . date('YmdHis');
        });
    }
    // protected $primaryKey = 'kode_batik';
    
    protected $guarded = ['id', 'created_at', 'updated_at'];


    public function kota()
    {
        return $this->belongsTo(kota::class);
    }
}
