<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kota extends Model
{
    protected $table = "kotas";
    protected $primaryKey = "id";
    protected $fillable = [
        'id','nama_kota'
    ];

    public function pengelolaan()
    {
        return $this->belongsToMany(pengelolaans::class,'nama_kota','id');
    }
}
