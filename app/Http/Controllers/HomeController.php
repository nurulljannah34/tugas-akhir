<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

use App\Models\User;
use App\Models\kota;
use App\Models\Batik;
use Illuminate\Support\Facades\Session as FacadesSession;

class HomeController extends Controller
{
    public function home()
    {
        return view('admin.home');
    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);
        $password = $user->password;
        return view('admin.profile', compact('user','password'));
    }

    public function lolaadmin($id)
    {
        $user = User::findOrFail($id);
        return view('admin.lolaadmin', compact('user'));
    }

    public function simpanedit(Request $request,$id)
    {
        $user = User::findOrFail($id);
        if ($request->password) {
            $save = $user->update([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password), // tambahkan hashing untuk password baru
            ]);
        } else {
            $save = $user->update([
                'name'=>$request->name,
                'email'=>$request->email,
            ]);
        }
        
        if ($save) {
            return redirect()->route('profile');
        }
        
    }

    public function motif()
    {
        $kota = kota::all();
        $batiks = Batik::all();
        // dd($batiks);
        return view('admin/motif', compact('kota','batiks'));
    }

    public function lolabatik()
    {
        $kota = kota::all();
        return view('admin/lolabatik', compact('kota'));
    }

    public function simpanbatik(Request $request)
    {
        $request->validate([
            'gambar_batik' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff' // 1096 = 1mb,
        ]);
        $file = $request->file('gambar_batik');
        $imageName = 'batik_' . uniqid() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('image'), $imageName);

        
        Batik::create([
            'nama_batik' => $request-> nama_batik,
            'motif_batik' => $request-> motif_batik,
            'deskripsi_batik' => $request-> deskripsi_batik,
            'asal_batik' => $request-> asal_batik,
            'gambar_batik' => $imageName,
        ]);
        Session::flash('message', 'Register Berhasil. Akun Anda sudah Aktif silahkan Login menggunakan username dan password.');
        return redirect('motif');
    }

    public function edit($id)
    {
        $batik = Batik::findOrFail($id);
        $kota = kota::all();
        return view('admin.editbatik', compact('batik','kota'));
    }

    public function simpaneditbatik(Request $request, $id)
    {
        
        $batik = Batik::where('kode_batik', $id)->firstOrFail();    
        
        
        if ($request->hasFile('gambar_batik')) {
            $request->validate([
                'gambar_batik' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff' // 1096 = 1mb,
            ]);
            $file = $request->file('gambar_batik');
            $imageName = 'batik_' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('image'), $imageName);
            $batik->gambar_batik = $imageName;
        }

        $save = $batik->update([
            'nama_batik' => $request->nama_batik,
            'motif_batik' => $request->motif_batik,
            'deskripsi_batik' => $request->deskripsi_batik,
            'asal_batik' => $request->asal_batik,
        ]);
        
        if ($save) {
            return redirect()->route('motif');
        }
        

    }

    public function hapus($id)
    {
        $batiks = Batik::findOrFail($id);
        $batiks->delete();
        return back();

    }


}
