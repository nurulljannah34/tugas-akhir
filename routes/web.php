<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Models\motif;
use App\Models\pengelolaan;
use App\Models\kota;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login',[LoginController::class,'login']);
Route::post('actionlogin',[LoginController::class,'actionlogin'])->name('actionlogin');
Route::get('logout',[LoginController::class,'logout'])->name('logout')->middleware('auth');

Route::get('home',[HomeController::class,'home'])->name('home')->middleware('auth');
Route::get('profile',[HomeController::class,'profile'])->name('profile')->middleware('auth');
Route::get('lolaadmin/{id}',[HomeController::class,'lolaadmin'])->name('lolaadmin')->middleware('auth');
Route::put('simpanedit/{id}',[HomeController::class,'simpanedit'])->name('simpanedit')->middleware('auth');

Route::get('motif',[HomeController::class,'motif'])->name('motif')->middleware('auth');
Route::get('lolabatik',[HomeController::class,'lolabatik'])->name('lolabatik')->middleware('auth');
Route::post('simpanbatik',[HomeController::class,'simpanbatik'])->name('simpanbatik')->middleware('auth');
Route::get('edit/{id}',[HomeController::class,'edit'])->name('edit')->middleware('auth');
Route::put('simpaneditbatik/{id}',[HomeController::class,'simpaneditbatik'])->name('simpaneditbatik')->middleware('auth');
Route::get('hapus/{id}',[HomeController::class,'hapus'])->name('hapus')->middleware('auth');


