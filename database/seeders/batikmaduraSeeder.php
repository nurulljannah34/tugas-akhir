<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\kota;
class batikmaduraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kota::create([
            'nama_kota'=>'Sumenep',
        ]);
        kota::create([
            'nama_kota'=>'Pamekasan',
        ]);
        kota::create([
            'nama_kota'=>'Sampang',
        ]);
        kota::create([
            'nama_kota'=>'Bangkalan',
        ]);
    }
}
