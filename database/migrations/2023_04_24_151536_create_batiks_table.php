<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batiks', function (Blueprint $table) {
        $table->id();
        $table->string('kode_batik');
        $table->string('nama_batik');
        $table->string('motif_batik');
        $table->text('deskripsi_batik');
        $table->unsignedBigInteger('asal_batik');
        $table->string('gambar_batik');
        $table->timestamps();
        $table->foreign('asal_batik')->references('id')->on('kotas')->onupdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batiks');
        
    }
}
